// CRUD Operations
// C - create.
// R - Retrieve
// U - Update
// D - Delete

// [SECTION] - Create (Insert documents)

/*
	SYNTAX: db.collectionName.insertOne({Object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09987654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// Insert Many
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "00102020",
			email: "stephhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
		{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 55,
		contact: {
			phone: "00102021",
			email: "firstmanonmoon@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}
]);

// [SECTION] - READ (finding documents)
/*
	db.collectionName.find();
	db.collectionName.find({field:value});
*/
// Will retrieve all documents
db.users.find();

db.users.find({firstName: "Stephen"});

// will receive a specific document
// db.users.find

// 
// 

// [SECTION] - UPDATE (Updating Documents)
// Syntax : db.collectionName.updateOne({criteria},  {$set: {field:value}});
db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00011",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "Active"
		}
	}
);

// Updatibg Multiple Documents

// Syntax: db.collectionName.updateMany({criteria}, {$set: {field:value}});

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}
);

// Replace one
db.users.replaceOne(
	{firstName: "Bill"},
	{firstName: "Mark"}

);


// Mini-Activity

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00011",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Tony",
			lastName: "Stark",
			age: 40,
			contact: {
				phone: "0010010110",
				email: "geniusbillionaire@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Head of Avengers",
			status: "deceased"
		}
	}
);

// [SECTION] - Delete
// Creating a document to delete

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Tony",
			lastName: "Stark",
			age: 40,
			contact: {
				phone: "0010010110",
				email: "geniusbillionaire@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Head of Avengers",
			status: "deceased"
		}
	}
);

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00011",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Peter StarLord",
			lastName: "Quill",
			age: 40,
			contact: {
				phone: "0010010110",
				email: "geniusbillionaire@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Head of Avengers",
			status: "deceased"
		}
	}
);

// Deleting a single document
// Syntax: db.collectionName.deleteOne

db.users.deleteOne({
	firstName: "Test"
});

db.users.deleteMany({
	firstName: "Stephen"
});


// Quesry an embedded document
db.users.find({
	contact:{
		phone: "00102020",
		email: "stephhawking@mail.com"
	}
});

// Query on nested field
db.users.find(
	{"contact.email": "janedoe@mail.com"}
);

// Queromg am array with exact elements
db.users.find(
	{courses: ["CSS", "Javascript", "Python"]}
	);

// Quering an array without regard to order
db.users.find({course: {$all: ["Javascript", "CSS", "Python"]}
});